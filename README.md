# createrepo_c

* Make sure createrepo_c is compiled with libmodulemd support, otherwise Koji's
bare repos won't work as expected and you'll have issues with modular packages.

* drpm support requires drpm-devel which is not provided by default on el7. This package is built internally to satisfy this dependency
